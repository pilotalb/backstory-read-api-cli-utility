Requirements
------------

1) Google API Python Client: pip install --user google-api-python-client
2) Backstory API JSON File - Contact your account team if you do not have this



Usage
-----

```
usage: api.py [-h] [-i IOC] [-il IOCLOOKUP] [-la] [-li] [-le]

optional arguments:
  -h, --help            show this help message and exit
  -i IOC, --ioc IOC     list assets that have accessed an IOC
  -il IOCLOOKUP, --ioclookup IOCLOOKUP
                        list details about an IOC
  -la, --listalerts     list alerts
  -li, --listiocs       list IOCs
  -le, --listevents     list events
```
  
Examples
--------

Hunting for IoCs:
```
$ python api.py -i bit.ly
{u'hostname': u'ws92817'}
{u'hostname': u'win-uuoc1orap8h'}
{u'hostname': u'desktop-7spk7ga'}
{u'hostname': u'ws19228'}
{u'assetIpAddress': u'10.50.22.253'}
{u'assetIpAddress': u'10.50.22.243'}
{u'assetIpAddress': u'10.50.22.210'}

Query time (secs): 0.81404709816



$ python api.py -i cb79d1f71cf9e2a72a25ef116618cb9c674a17e1e57565676abb1cfb33a4cebf
{u'hostname': u'test-server'}

Query time (secs): 0.468773126602
```
  