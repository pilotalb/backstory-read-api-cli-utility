# Imports required for the sample - Google Auth and API Client Library Imports
# Get these packages from https://pypi.org/project/google-api-python-client/
# or run $ pip install google-api-python-client from your terminal
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import argparse
import datetime
import json
import os
import re
import sys
import time
from googleapiclient import _auth
from google.oauth2 import service_account

def main():

  # Handle arguments
  parser = argparse.ArgumentParser()
  parser.add_argument('-i', '--ioc', help="""list assets that have accessed
    an IOC""")
  parser.add_argument('-il', '--ioclookup', help='list details about an IOC')
  parser.add_argument('-la', '--listalerts', action='store_true',
                      help='list alerts')
  parser.add_argument('-li', '--listiocs', action='store_true',
                      help='list IOCs')
  parser.add_argument('-le', '--listevents', action='store_true',
                      help='list events')
  args = parser.parse_args()

  # If we're searching for an ioc
  if args.ioc:
    call_list_assets()
  if args.ioclookup:
    call_list_ioc_details()
  if args.listalerts:
    call_list_alerts()
  if args.listiocs:
    call_list_iocs()
  if args.listevents:
    call_list_events()


def call_list_assets():
  """List all assets that accessed an artifact.

  For your enterprise, given the specified artifact, list all of the assets
  that accessed it within the specified time period, including the first and
  last time those assets accessed the artifact.

  """

  # Constants
  scopes = ['https://www.googleapis.com/auth/chronicle-backstory']
  service_account_file = os.path.join(os.environ['HOME'], 'bk_credentials.json')
  # Or the location you placed your JSON file.

  # Create a credential using Google Developer Service Account Credential and
  # Backstory API Scope
  credentials = service_account.Credentials.from_service_account_file(
      service_account_file, scopes=scopes)

  # Build a Http client that can make authorized OAuth requests.
  http_client = _auth.authorized_http(credentials)

  # The API support searching by IP, domain name, md5, sha1, sha256
  #  Let's use regex to determine which type of IOC we received, as we'll need
  # this in the API call
  ioc = sys.argv[2]
  md5 = re.match('^[a-fA-F0-9]{32}$', ioc)
  sha1 = re.match('^[a-fA-F0-9]{40}$', ioc)
  sha256 = re.match('^[a-fA-F0-9]{64}$', ioc)
  ipaddr = re.match(r'^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}'
                    '([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$', ioc)
  domain = re.match(r'^(?=.{1,255}$)(?!-)[A-Za-z0-9\-]{1,63}(\.[A-Za-z0-9\-]'
                    r'{1,63})*\.?(?<!-)$', ioc)
  if md5:
    ioc_api = 'hash_md5'
  elif sha1:
    ioc_api = 'hash_sha1'
  elif sha256:
    ioc_api = 'hash_sha256'
  elif ipaddr:
    ioc_api = 'destination_ip_address'
  elif domain:
    ioc_api = 'domain_name'
  else:
    print('Not a valid IOC' + ioc)
    exit(1)

  # Get the current zulu time for polling the API
  end_time = datetime.datetime.utcnow()
  end_time = end_time.strftime('%Y-%m-%dT%H:%M:00Z')

  # Construct the URL
  backstory_api_v1_url = 'https://backstory.googleapis.com/v1'
  list_assets_url = (backstory_api_v1_url + '/artifact/listassets?'
                     'start_time=2015-01-01T00:00:00Z&end_time=' + end_time +
                     '&artifact.' + ioc_api + '=' + ioc)

  # Track the time it takes to execute the query
  start_time = time.time()

  # Make a request
  response = http_client.request(list_assets_url, 'GET')

  # Parse the response
  if response[0].status == 200:
    iocs = response[1]
    # List of IoCs are returned for further processing
    # print(iocs)
    print_formatted(iocs)
  else:
    # something went wrong, please see the response detail
    err = response[1]
    print(err)
  elapsed_time = time.time() - start_time
  print('\nQuery time (secs): ' + format(elapsed_time))


def print_formatted(contents):
  """Print formatted version of results."""

  # Check to see if we have matches
  if 'assets' not in str(contents):
    print('Indicator not found\n')
    exit(1)

  # Load the API output as JSON
  js = json.loads(contents)

  # Loops through the matches
  for devices in js['assets']:
    print(devices['asset'])


def call_list_ioc_details():
  """Return any threat intelligence associated with an artifact.

  Use this method to submit an artifact indicator and return any threat
  intelligence associated with that artifact. The threat intelligence
  information is drawn from your enterprise security systems and from
  Chronicle's IoC partners (for example, the DHS threat feed).

  """

  # Constants
  scopes = ['https://www.googleapis.com/auth/chronicle-backstory']
  service_account_file = os.path.join(os.environ['HOME'], 'bk_credentials.json')
  # Or the location you placed your JSON file.

  # Grab the IOC from arguments
  ioc = sys.argv[2]

  # The API support searching by IP, domain name, md5, sha1, sha256sh
  # Let's use regex to determine which type of IOC we received, as we'll need
  # this in the API call
  ioc = sys.argv[2]
  md5 = re.match('^[a-fA-F0-9]{32}$', ioc)
  sha1 = re.match('^[a-fA-F0-9]{40}$', ioc)
  sha256 = re.match('^[a-fA-F0-9]{64}$', ioc)
  ipaddr = re.match(r'^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}'
                    '([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$', ioc)
  domain = re.match(r'^(?=.{1,255}$)(?!-)[A-Za-z0-9\-]{1,63}(\.[A-Za-z0-9\-]'
                    r'{1,63})*\.?(?<!-)$', ioc)
  if md5:
    ioc_api = 'hash_md5'
  elif sha1:
    ioc_api = 'hash_sha1'
  elif sha256:
    ioc_api = 'hash_sha256'
  elif ipaddr:
    ioc_api = 'destination_ip_address'
  elif domain:
    ioc_api = 'domain_name'
  else:
    print('Not a valid IOC' + ioc)
    exit(1)

  # Create a credential using Google Developer Service Account Credential and
  # Backstory API Scope.
  credentials = service_account.Credentials.from_service_account_file(
      service_account_file, scopes=scopes)

  # Build a Http client that can make authorized OAuth requests.
  http_client = _auth.authorized_http(credentials)

  # Construct the URL
  backstory_api_v1_url = 'https://backstory.googleapis.com/v1'
  list_ioc_details_url = '{}/artifact/listiocdetails?artifact.{}={}'.format(
      backstory_api_v1_url, ioc_api, ioc)

  # Make a request
  response = http_client.request(list_ioc_details_url, 'GET')

  # Parse the response
  if response[0].status == 200:
    iocs = response[1]
    # List of Sources are returned for further processing
    print(iocs)
  else:
    # something went wrong, please see the response detail
    err = response[1]
    print(err)


def call_list_alerts():
  """List all of the 3rd party security alerts tracked within your enterprise.

  """

  # Constants
  scopes = ['https://www.googleapis.com/auth/chronicle-backstory']
  service_account_file = os.path.join(os.environ['HOME'], 'bk_credentials.json')
  # Or the location you placed your JSON file.

  # Create a credential using Google Developer Service Account Credential and
  # Backstory API Scope.
  credentials = service_account.Credentials.from_service_account_file(
      service_account_file, scopes=scopes)

  # Build a Http client that can make authorized OAuth requests.
  http_client = _auth.authorized_http(credentials)

  # Construct the URL
  backstory_api_v1_url = 'https://backstory.googleapis.com/v1'
  list_alerts_url = (
      '{}/alert/listalerts?start_time=2015-01-01T00:00:00Z'
      '&end_time=2019-12-31T00:00:00Z&page_size=1').format(backstory_api_v1_url)

  # Make a request
  response = http_client.request(list_alerts_url, 'GET')

  # Parse the response
  if response[0].status == 200:
    iocs = response[1]
    # List of Alerts are returned for further processing
    print(iocs)
  else:
    # something went wrong, please see the response detail
    err = response[1]
    print(err)


def call_list_iocs():
  """List all of the IoCs discovered within your enterprise.

  List all of the IoCs discovered within your enterprise within the specified
  time range. If you receive the maximum number of IoCs you specified using the
  page_size parameter (or 10,000, the default), there might still be more IoCs
  discovered in your Backstory account. You might want to narrow the time range
  and issue the call again to ensure you have visibility on all possible IoCs.

  """

  # Constants
  scopes = ['https://www.googleapis.com/auth/chronicle-backstory']
  service_account_file = os.path.join(os.environ['HOME'], 'bk_credentials.json')
  # Or the location you placed your JSON file.

  # Create a credential using Google Developer Service Account Credential and
  # Backstory API Scope.
  credentials = service_account.Credentials.from_service_account_file(
      service_account_file, scopes=scopes)

  # Build a Http client that can make authorized OAuth requests.
  http_client = _auth.authorized_http(credentials)

  # Construct the URL
  backstory_api_v1_url = 'https://backstory.googleapis.com/v1'
  list_iocs_url = ('{}/ioc/listiocs?start_time=2019-10-15T20:37:00Z'
                   '&page_size=1').format(backstory_api_v1_url)

  # Make a request
  response = http_client.request(list_iocs_url, 'GET')

  # Parse the response
  if response[0].status == 200:
    iocs = response[1]
    # List of IoCs are returned for further processing
    print(iocs)
  else:
    # something went wrong, please see the response detail
    err = response[1]
    print(err)


def call_list_events():
  """List all of the events from a specific asset and timeframe.

  List all of the events discovered within your enterprise within the specified
  time range. If you receive the maximum number of events you specified using
  the page_size parameter (or 10,000, the default), there might still be more
  events within your Backstory account. You can narrow the time range and issue
  the call again to ensure you have visibility into all possible events.


  """

  # Constants
  scopes = ['https://www.googleapis.com/auth/chronicle-backstory']
  service_account_file = os.path.join(os.environ['HOME'], 'bk_credentials.json')
  # Or the location you placed your JSON file.

  # Create a credential using Google Developer Service Account Credential and
  # Backstory API Scope.
  credentials = service_account.Credentials.from_service_account_file(
      service_account_file, scopes=scopes)

  # Build a Http client that can make authorized OAuth requests.
  http_client = _auth.authorized_http(credentials)

  # Construct the URL
  backstory_api_v1_url = 'https://backstory.googleapis.com/v1'
  list_events_url = ('{}/asset/listevents?start_time=2019-02-22T10:00:00Z'
                     '&page_size=10&end_time=2019-02-22T11:00:00Z&'
                     'reference_time=2019-02-22T11:00:00Z&'
                     'asset.hostname=todd-fields-pc'
                    ).format(backstory_api_v1_url)

  # Make a request
  response = http_client.request(list_events_url, 'GET')

  # Parse the response
  if response[0].status == 200:
    events = response[1]
    # List of events are returned for further processing
    print(events)
  else:
    # something went wrong, please see the response detail
    err = response[1]
    print(err)


if __name__ == '__main__':
  main()
